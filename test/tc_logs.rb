#!/usr/bin/ruby -w

$:.unshift File.join(File.dirname(__FILE__), '..', 'lib')

Encoding.default_external = 'UTF-8'

require 'test/unit'
require 'collab-qa'

class LogsTest < Test::Unit::TestCase
  if File::directory?('test/source')
    SRCDIR = 'test/source'
    DSTDIR = 'test/dest'
  elsif File::directory?('source')
    SRCDIR = 'source'
    DSTDIR = 'dest'
  else
    raise 'source directory not found.'
  end

  def test_parser
    allok = true
    Dir.foreach(SRCDIR).sort.each do |f|
      next if f !~ /log$/
      next if ENV['SOURCE'] != nil and ENV['SOURCE'] != f
      puts "Checking #{f}"
      log = CollabQA::Log::new(SRCDIR + '/' + f)
      log.guess_failed
      log.extract_log
      str = log.to_s
      if File::exist?(DSTDIR + '/' + f.gsub(/log$/, 'output'))
        output = File::read(DSTDIR + '/' + f.gsub(/log$/, 'output'))
        if output != str
          File::open(DSTDIR + '/' + f.gsub(/log$/, 'output.new'), "w") do |fd|
            fd.print(str)
          end
          puts "Test failed for #{f}."
          puts "  Check: diff -u #{DSTDIR + '/' + f.gsub(/log$/, 'output')}{,.new}"
          puts "  Commit: mv -f #{DSTDIR + '/' + f.gsub(/log$/, 'output')}{.new,}"
          allok = false
        end
      else
        puts "Missing #{DSTDIR + '/' + f.gsub(/log$/, 'output')}. Writing it, but check manually!"
        File::open(DSTDIR + '/' + f.gsub(/log$/, 'output'), "w") do |fd|
          fd.print(str)
        end
        allok = false
      end
    end
    assert(allok)
  end
end
