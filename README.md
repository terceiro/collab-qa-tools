This repository contains tools for managing large-scale QA tests, such as the archive rebuilds performed on AWS.

The overall process (detailed below) looks like:

1. reserve nodes on AWS, using cqa-ec2
2. generate a list of tasks, using lib/collab-qa/tasks_generator.rb
3. start the process, using cqa-masternode
4. wait
5. fetch the results, using cqa-fetch-results
6. process the results (file bugs), using cqa-scanlogs and cqa-annotate

Note: all those tools are not really release-quality. Most notably, many things are hardcoded.

# Reserving nodes

Read/change the top of `bin/cqa-ec2` where all parameters are defined.

Look at `bin/cqa-setup-rebuildnode` (script that is run on the nodes)

This uses ruby-fog-aws. Configure `~/.fog`

`bin/cqa-ec2 help`

`bin/cqa-ec2 create_and_setup`

`bin/cqa-ec2 create_and_setup --nb=50`

check VMs with `bin/cqa-ec2 list-all`

`bin/cqa-ec2 list > nodes`

# Generating the list of tasks

See examples in `lib/collab-qa/tasks_generator.rb`

One example:
```
ruby -Ilib -rcollab-qa -e "
tasks = CollabQA::TasksGenerator::new('rebuild-full').generate({'exclude-not-testing' => true})
tasks = tasks.select { |t| t['esttime'] && t['esttime'] < 30 }.shuffle[0...100]
puts JSON::pretty_generate(tasks)
" > tasks.json
```

# Start the process and wait

* Copy `nodes` and `tasks.json` to the machine where you will run cqa-masternode
* Run cqa-mastermode, with, for example: `bin/cqa-masternode -n nodes -t tasks.json -s 3 -o log -d /tmp/logs.test --no-halt` (see `cqa-masternode -h`)
* You can watch the log, and `bin/cqa-ec2 list-all` to see nodes that are still running

# Fetch the results

* make sure that all instances are terminated
* cd to `collab-qa-data` directory
* run: `DATE=2018/12/29 bin/cqa-fetch-results -d /tmp/logs.test`
* then follow the instructions

# Process results

Use `cqa-scanlogs` and `cqa-annotate`
